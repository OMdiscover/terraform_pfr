# Appel du module principal

module "principal" {
    source = "./modules"
    external_network = "external"
    router_name = "router_tf_fr"
    network_name = "network_tf_fr"
    subnet_name = "subnet_tf_fr"
    subnet_ip_range = "192.168.61.0/24"
}