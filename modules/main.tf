# CREATION INSTANCES

resource "openstack_compute_instance_v2" "bastion" {
  name            = "bastion"
  image_name  = "imta-ubuntu20"
  flavor_name = "m1.medium"
  key_pair        = "demo"
  security_groups = ["secgroup_bastion"]
  network {
    name = var.network_name
  } 


  depends_on = [
        openstack_networking_subnet_v2.internal_subnet,
        openstack_networking_secgroup_v2.secgroup_bastion,
    ]
  provisioner "local-exec" {
    command = "echo ${openstack_compute_instance_v2.bastion.access_ip_v4}  >> ip_internes.txt"
  }
}
  
resource "time_sleep" "wait_90_seconds" {
  depends_on = [openstack_compute_instance_v2.bastion]
  create_duration = "90s"
}
resource "null_resource" "hello" {
# ...

depends_on = [ time_sleep.wait_90_seconds ]
  triggers = {
    always-update =  timestamp()
  }

connection {
    type     = "ssh"
    user     = "ubuntu"
    private_key = file("modules/demo.pem")
    host     =  "${openstack_networking_floatingip_v2.admin.address}"
  }
provisioner "local-exec" {
interpreter = ["/bin/bash","-c"]
command = <<EOT
ls
EOT
}

provisioner "file" {
    source      = "ip_internes.txt"
    destination = "/tmp/ip_internes.txt"
  }



}


  


resource "openstack_compute_instance_v2" "app-server" {
    for_each = toset(["node01","node02","node03"])
    name         =  each.key
    image_name   = "imta-ubuntu20"
    flavor_name  = "m1.medium" 
    key_pair        = "demo"
    security_groups = ["secgroup_application"]
    network {
    name = var.network_name
  }
  depends_on = [
        openstack_networking_subnet_v2.internal_subnet,
        openstack_networking_secgroup_v2.secgroup_application,
    ]

    provisioner "local-exec" {
    command = "echo ${self.access_ip_v4}  >> ip_internes.txt"
  }


}

# Adresses ip

resource "openstack_networking_floatingip_v2" "admin" {
  pool = var.external_network
  description = "admin"
}

resource "openstack_networking_floatingip_v2" "application" {
  pool = var.external_network
  description = "application"
}

resource "openstack_compute_floatingip_associate_v2" "admin" {
  floating_ip = "${openstack_networking_floatingip_v2.admin.address}"
  instance_id = "${openstack_compute_instance_v2.bastion.id}"
  depends_on = [
        openstack_compute_instance_v2.bastion,
    ]
}

resource "openstack_compute_floatingip_associate_v2" "application" {
  floating_ip = "${openstack_networking_floatingip_v2.application.address}"
  instance_id = "${openstack_compute_instance_v2.app-server["node01"].id}"
  depends_on = [
        openstack_compute_instance_v2.app-server,
    ]
}

resource "openstack_networking_network_v2" "internal_net" {
  name                = var.network_name
  admin_state_up      = true
}

resource "openstack_networking_subnet_v2" "internal_subnet" {
  name            = var.subnet_name
  network_id      = "${openstack_networking_network_v2.internal_net.id}"
  ip_version      = 4
  cidr            = var.subnet_ip_range
}

resource "openstack_networking_router_v2" "internal_router" {
  name                = var.router_name
  external_network_id = "${ data.openstack_networking_network_v2.ext_network.id}"
  depends_on = [
        openstack_networking_subnet_v2.internal_subnet,
    ]
}

# ===
# Create Interface between router and subnet
# ===

resource "openstack_networking_router_interface_v2" "internal_router_interface_1" {
  router_id = "${openstack_networking_router_v2.internal_router.id}"
  subnet_id = "${openstack_networking_subnet_v2.internal_subnet.id}"
  depends_on = [
        openstack_networking_router_v2.internal_router,
    ]
}

resource "openstack_networking_secgroup_v2" "secgroup_bastion" {
  name        = "secgroup_bastion"
  description = "group pour bastion"

}

resource "openstack_networking_secgroup_v2" "secgroup_application" {
  name        = "secgroup_application"
  description = "groupe pour application"
 
}


resource "openstack_networking_secgroup_rule_v2" "secgroup_rule_bastion" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 22
  port_range_max    = 22
  security_group_id = "${openstack_networking_secgroup_v2.secgroup_bastion.id}"
}

resource "openstack_networking_secgroup_rule_v2" "secgroup_rule_application" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 80
  port_range_max    = 80
  security_group_id = "${openstack_networking_secgroup_v2.secgroup_application.id}"
}


