variable "external_network" {
  type    = string
  default = "external"
}

variable "router_name" {
  type    = string
  default = "router_tf_fr"
}

variable "network_name" {
  type    = string
  default = "network_tf_fr"
}

variable "subnet_name" {
  type    = string
  default = "subnet_tf_fr"
}

variable "subnet_ip_range" {
  type    = string
  default = "192.168.61.0/24"
}
