output "ip_adress_bastion" {
  description = "adresse ip de l'instance bastion"
  value       =  openstack_compute_instance_v2.bastion.access_ip_v4
}

output "ip_adress_node01" {
  description = "adresse ip de node01"
  value       =  openstack_compute_instance_v2.app-server["node01"].access_ip_v4
}

output "ip_adress_node02" {
  description = "adresse ip de node02"
  value       =  openstack_compute_instance_v2.app-server["node02"].access_ip_v4
}

output "ip_adress_node03" {
  description = "adresse ip de node03"
  value       =  openstack_compute_instance_v2.app-server["node03"].access_ip_v4
}