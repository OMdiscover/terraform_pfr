# terraform_PFR


---
Ce dépôt est constitué de fichiers Terraform dans le but de déployer rapidement des ressources composées de 4 instances, un réseau et sous-réseau, un routeur.

## Initialisation
---
* Il est nécessaire de clôner le projet sur votre machine en locale pour déployer les ressources, il faut ensuite copier sa clé ssh dans le dossier et indiquer son nom dans le fichier principal.tf dans la partie de création des ressources.
* Après avoir clôné le dépot, et initialiser le projet, vous pouvez ensuite
* écrire le code suivant pour pouvoir effectuer un déploiement des ressources:
* Pour l'api:
``` yaml
terraform apply
```
* Pour l'infrastructure créee:
``` yaml
terraform destroy
```
## Variables

Les variables utilisées pour codifier cette infrastructure sont déclarées dans le fichier variables.tf. Les vcariables utilisées sont:

* external_network : pour le nom du réseau external
* router_name : pour le nom du routeur
* network_name : pour le nom du réseau 
* subnet_name : pour le nom du sous-réseau
* subnet_ip_range : pour la plage d'adresses